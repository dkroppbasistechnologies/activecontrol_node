A set of Node-RED nodes to interact with ActiveControl SOAP API

[![Apache License][license-image]][license-url]
# ActiveControl SOAP Node-RED nodes

> Allows access to [ActiveControl](https://www.basistechnologies.com/products/activecontrol/) SOAP API from [Node-RED](https://nodered.org/).

Documentation and sample flows are accessible through the [Node-RED](https://nodered.org/) UI.

[license-image]: https://img.shields.io/badge/License-Apache%202.0-blue.svg
[license-url]: https://bitbucket.org/basistech/activecontrol_node/src/master/LICENSE

## Field mapping template nodes
You can import pre-configured node templates into your flow that facilitate the mapping of fields.

![Screenshot](./resources/readme-screenshot-1.png)