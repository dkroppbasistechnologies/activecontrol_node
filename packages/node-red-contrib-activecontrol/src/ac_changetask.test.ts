import helper from "node-red-node-test-helper"
import { config } from "dotenv"
import { ChangeTaskNode } from "./changeTaskDefinitions"
// eslint-disable-next-line @typescript-eslint/no-var-requires
const ac_config = require("./nodes/ac_config_node")
const changetask = require("./nodes/changeTaskNode")

config()

beforeEach(done => helper.startServer(done))
afterEach(done => {
  helper.unload()
  helper.stopServer(done)
})

test("node registration", done => {
  const flow = [
    {
      id: "change",
      type: "change-task",
      name: "changeTask",
      activecontrol: "ac",
      wires: [["n2"]]
    },
    { id: "ac", type: "ac_config", name: "test name", url: "http://foo" },
    { id: "n2", type: "helper" }
  ]
  helper.load([ac_config, changetask], flow, function () {
    const n2 = helper.getNode("change") as ChangeTaskNode

    expect(n2).toBeDefined()
    expect(n2.name).toBe("changeTask")
    expect(n2.activecontrol?.id).toBe("ac")
    expect(n2.activecontrol?.config?.url).toBe("http://foo")
    done()
  })
})
