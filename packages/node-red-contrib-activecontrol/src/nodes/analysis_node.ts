import { NodeAPI } from "node-red"
import {
  mergeOptions,
  ActiveControlSoapClient,
  isStartAnalysisRequest,
  StartAnalysisResponse
} from "@basistechnologies/activecontrol-soap-api"
import { AcConfigNode } from "../ac_config_definitions"
import { errorText, waitmillis } from "./util"
import {
  isTaskAnalysisMessage,
  mergeAnalysisOptions,
  TaskAnalysisNode,
  TaskAnalysisNodeDef
} from "../taskAnalysisDefinitions"

const tryReadResultsUntil = async (
  client: ActiveControlSoapClient,
  maxwait: string,
  anlrun: StartAnalysisResponse,
  XMaxlines = "10"
) => {
  const mw = parseInt(maxwait)
  if (!mw || mw === NaN) return anlrun
  const limit = 1000 * mw + new Date().getTime()
  let endTime
  let response
  do {
    endTime = new Date().getTime()
    response = await client.readAnalysisResults({
      XAnalysisId: anlrun.YAnalysisId,
      XMaxlines
    })
    await waitmillis(1000)
  } while (endTime < limit && response.isRunning)
  return response
}

module.exports = (RED: NodeAPI) => {
  function createTaskAnalysisNode(
    this: TaskAnalysisNode,
    nodeDef: TaskAnalysisNodeDef
  ) {
    const node = this
    // Create node based on the passed instance definition
    RED.nodes.createNode(node, nodeDef)
    node.maxWaitResults = nodeDef.maxWaitResults
    node.maxLines = nodeDef.maxLines

    // Get the AC configuration and apply it
    node.activecontrol = RED.nodes.getNode(
      nodeDef.activecontrol
    ) as AcConfigNode

    // check if a config node is given and valid
    if (!node.activecontrol) throw new Error("Configuration node missing")
    if (!node.activecontrol.config || !node.activecontrol.credentials)
      throw new Error("Configuration node not set up properly")

    node.on("input", async function (msg, send, done) {
      try {
        if (!isTaskAnalysisMessage(msg))
          throw new Error("Message format not recognized")

        // Check if options were overridden in this node and merge them
        const {
          url,
          password,
          systemnumber,
          username,
          maxWaitResults,
          maxLines
        } = msg
        const config = {
          ...node.activecontrol!.config!,
          ...node.activecontrol!.credentials,
          maxWaitResults: node.maxWaitResults,
          maxLines: node.maxLines
        }
        const options = mergeAnalysisOptions(config, {
          url,
          password,
          systemnumber,
          username,
          maxWaitResults,
          maxLines
        })

        // Make the call
        const client = new ActiveControlSoapClient(options)
        if (isStartAnalysisRequest(msg.payload)) {
          node.status({ fill: "blue", text: "Analysing task..." })
          const analysisRun = await client.startAnalysis(msg.payload)
          const runorRes = await tryReadResultsUntil(
            client,
            options.maxWaitResults,
            analysisRun,
            options.maxLines
          )
          send({ ...msg, payload: runorRes })
        } else {
          node.status({ fill: "blue", text: "Reading analysis results..." })
          const result = await client.readAnalysisResults(msg.payload)
          send({ ...msg, payload: result })
        }

        node.status("")
        done()
      } catch (error) {
        node.status({ fill: "red", text: errorText(error) })
        done(error)
      }
    })
  }
  RED.nodes.registerType("task-analysis", createTaskAnalysisNode)
}
