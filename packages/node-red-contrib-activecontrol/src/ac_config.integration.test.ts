import helper from "node-red-node-test-helper"
import { AcConfigNode } from "./ac_config_definitions"
// eslint-disable-next-line @typescript-eslint/no-var-requires
const ac_config = require("./nodes/ac_config_node")

beforeEach(done => helper.startServer(done))
afterEach(done => {
  helper.unload()
  helper.stopServer(done)
})

test("node registration", done => {
  const flow = [
    { id: "n1", type: "ac_config", name: "test name", url: "http://foo" }
  ]
  helper.load(ac_config, flow, function () {
    const n1 = helper.getNode("n1") as AcConfigNode
    expect(n1.name).toBe("test name")
    expect(n1.config?.url).toBe("http://foo")
    done()
  })
})
