import { Options, Task } from "@basistechnologies/activecontrol-soap-api"
import { NodeDef, Node, NodeMessage } from "node-red"
import { AcConfigNode } from "./ac_config_definitions"

export interface CreateTaskNodeDef extends NodeDef {
  activecontrol: string
}

export interface CreateTaskNode extends Node {
  activecontrol?: AcConfigNode
}
export interface CreateTaskNodeMessage extends NodeMessage, Partial<Options> {
  payload: Partial<Task>
}
/**
 * check if message is a valid task creation message
 * @param msg a node message
 *
 * as most fields are optional, we only require that payload has
 * either a reference or a caption
 * server will complain if mandatory fields are missing
 */
export function isCreateTaskMessage(
  msg: NodeMessage
): msg is CreateTaskNodeMessage {
  const { payload } = msg as any
  return "REFERENCE" in payload || "CAPTION" in payload
}
