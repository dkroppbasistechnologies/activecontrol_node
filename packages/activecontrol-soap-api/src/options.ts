export interface Options {
  url: string
  systemnumber: string
  password?: string
  authorization?: string
  username?: string
}

export function validateOptions(options: Options) {
  if (!options.url?.match(/^https?:\/\//i))
    throw new Error(`Invalid AC Url provided:${options.url}`)
  if (!options.systemnumber?.match(/^\d\d$/))
    throw new Error("Invalid system number,must be a 2 digit number")
  if (!options.authorization && !(options.username && options.password))
    throw new Error("No authentication provided in configuration")
}

export function mergeOptions(
  rawOptions: Options,
  incomingOptions: Partial<Options>
): Options {
  const {
    systemnumber,
    url,
    authorization,
    password,
    username
  } = incomingOptions
  const options = {
    systemnumber: systemnumber || rawOptions.systemnumber,
    url: url || rawOptions.url,
    authorization: authorization || rawOptions.authorization,
    password: password || rawOptions.password,
    username: username || rawOptions.username
  }
  validateOptions(options)
  return options
}

// export const errorText = (error: any, defaultmsg = "unknown error") =>
//   (error && (error.message || error.toString())) || defaultmsg
