import { config } from "dotenv"
import { env } from "process"
import { ActiveControlSoapClient } from "../index"
import { TaskChangeData } from "../taskDefinitions"

config()
const readConfigFromEnv = () => ({
  url: env.AC_SERVER || "",
  username: env.AC_USER,
  password: env.AC_PASSWORD,
  systemnumber: env.AC_SYSTEM_NUMBER || ""
})

const createReadTaskClient = () =>
  new ActiveControlSoapClient(readConfigFromEnv())

test("changing task groupid and priority", async () => {
  if (!env.AC_ENABLE_WRITE) return
  // first read the task
  const client = createReadTaskClient()
  const payload = {
    XTaskid: env.AC_CHANGE_TASKID || ""
  }
  const task = await client.readTask(payload)
  expect(task.ID).toBe(payload.XTaskid)
  expect(task.REFERENCE).toBeTruthy()
  expect(Array.isArray(task.testers)).toBe(true)
  const groupid =
    task.GROUPID === env.AC_CHANGE_GROUPID1
      ? env.AC_CHANGE_GROUPID2 || ""
      : env.AC_CHANGE_GROUPID1 || ""
  const priority = task.PRIORITY === "1" ? "2" : "1"
  const chdata: TaskChangeData = {
    XTask: { ...task, PRIORITY: priority, GROUPID: groupid },
    XUpddateTask:   true,
    XUpdateCustfields:   false,
    XUpdateDesc:   false,
    XUpdateTesters:   false
  }
  const result = await client.changeTask(chdata)
  expect(result.message).toBe("Task changed successfully")
  const changedtask = await client.readTask(payload)
  expect(changedtask.GROUPID).toBe(groupid)
  expect(changedtask.PRIORITY).toBe(priority)
})
