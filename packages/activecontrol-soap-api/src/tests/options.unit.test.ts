import { mergeOptions } from "../options"

test("merge config 1", () => {
    const options = mergeOptions(
      {
        systemnumber: "00",
        url: "http://foo",
        username: "foo",
        password: "foopwd",
        authorization: "fooauth"
      },
      {
        systemnumber: "01"
      }
    )
    expect(options.systemnumber).toBe("01")
    expect(options.url).toBe("http://foo")
    expect(options.username).toBe("foo")
    expect(options.password).toBe("foopwd")
    expect(options.authorization).toBe("fooauth")
  })
  
  test("merge config 2", () => {
    const options = mergeOptions(
      {
        systemnumber: "00",
        url: "http://foo",
        username: "foo",
        password: "foopwd",
        authorization: "fooauth"
      },
      {
        systemnumber: "01",
        url: "http://bar",
        username: "bar",
        password: "barpwd",
        authorization: "barauth"
      }
    )
    expect(options.systemnumber).toBe("01")
    expect(options.url).toBe("http://bar")
    expect(options.username).toBe("bar")
    expect(options.password).toBe("barpwd")
    expect(options.authorization).toBe("barauth")
  })
  