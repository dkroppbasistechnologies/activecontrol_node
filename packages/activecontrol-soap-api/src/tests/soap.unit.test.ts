import { stripEnvelope } from "../soap"
import { parse } from "fast-xml-parser"
const exampleSoapEnvelope = `
<soap-env:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:sap-com:document:sap:soap:functions:mc-style">
  <soap-env:Header/>
  <soap-env:Body>
    <urn:_-bti_-teTaskReadWs>
      <XSystemNumber>18</XSystemNumber>
      <XTaskid>10017102000000001942</XTaskid>
    </urn:_-bti_-teTaskReadWs>
  </soap-env:Body>
</soap-env:Envelope>`

describe("soap utility", () => {
  it("should strip the envelope", done => {
    const data = stripEnvelope(parse(exampleSoapEnvelope))

    expect(data).toStrictEqual(
      parse(`<urn:_-bti_-teTaskReadWs>
        <XSystemNumber>18</XSystemNumber>
        <XTaskid>10017102000000001942</XTaskid>
      </urn:_-bti_-teTaskReadWs>`)
    )
    done()
  })
})
