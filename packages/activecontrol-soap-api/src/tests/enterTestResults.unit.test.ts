import { config } from "dotenv"
import { env } from "process"
import { ActiveControlSoapClient } from "../index"
import { TestResultCodes } from "../taskDefinitions"


config()
const readConfigFromEnv = () => ({
  url: env.AC_SERVER || "",
  username: env.AC_USER,
  password: env.AC_PASSWORD,
  systemnumber: env.AC_SYSTEM_NUMBER || ""
})
const createTestResultsClient = () =>
  new ActiveControlSoapClient(readConfigFromEnv())

test("enter test results", async () => {
  const client = createTestResultsClient()
  const payload = {
    XClose: !!env.AC_CLOSE,
    XComment: env.AC_COMMENT || "",
    XRescode: (env.AC_RESCODE as TestResultCodes) || TestResultCodes.Passed,
    XTarget: env.AC_TARGET || "",
    XTaskid: env.AC_TASKID || ""
  }

  if (!env.AC_ENABLE_WRITE) return
  const result = await client.enterTestResults(payload)
  expect(result.resultsId).toBeTruthy()

  expect(result.message).toBeTruthy()
})

test("enter test results - failure", async () => {
  const client = createTestResultsClient()
  const payload = {
    XClose: !!env.AC_CLOSE,
    XComment: env.AC_COMMENT || "",
    XRescode: "Test failed!" as TestResultCodes,
    XTarget: env.AC_TARGET || "",
    XTaskid: env.AC_TASKID || ""
  }

  try {
    await client.enterTestResults(payload)
    fail()
  } catch (error) {
    expect(error).toBeTruthy()
    expect(error.toString()).toBeTruthy()
  }
})
