import { config } from "dotenv"
import { env } from "process"
import { ActiveControlSoapClient } from "../index"

config()
const readConfigFromEnv = () => ({
  url: env.AC_SERVER || "",
  username: env.AC_USER,
  password: env.AC_PASSWORD,
  systemnumber: env.AC_SYSTEM_NUMBER || ""
})

const createReadTaskClient = () =>
  new ActiveControlSoapClient(readConfigFromEnv())

test("reading task", async () => {
  const client = createReadTaskClient()
  const payload = {
    XTaskid: env.AC_TASKID || ""
  }

  const task = await client.readTask(payload)
  expect(task.ID).toBe(payload.XTaskid)
  expect(task.REFERENCE).toBeTruthy()
  expect(Array.isArray(task.testers)).toBe(true)
})

test("reading task - failure", async () => {
  const client = createReadTaskClient()
  const payload = {
    XTaskid: "123"
  }

  try {
    const task = await client.readTask(payload)
    fail()
  } catch (error) {
    expect(error).toBeTruthy()
    expect(error.toString()).toBeTruthy()
  }
})
