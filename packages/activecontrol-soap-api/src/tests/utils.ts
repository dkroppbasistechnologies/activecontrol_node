import { config } from "dotenv"
import { env } from "process"
import { ActiveControlSoapClient } from "../index"

config()
const readConfigFromEnv = () => ({
  url: env.AC_SERVER || "",
  username: env.AC_USER,
  password: env.AC_PASSWORD,
  systemnumber: env.AC_SYSTEM_NUMBER || ""
})

export const createClient = () =>
  new ActiveControlSoapClient(readConfigFromEnv())

export const readEnv = (name: string) => env[name] || ""
