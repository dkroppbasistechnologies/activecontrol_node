import { Client } from "./client"

export type LibraryEntry = {
  // represents the DB value
  path: string
  data: {
    meta: object
    body: object
  }
}

export class Library {
  private _client: Client | undefined

  set client(client: Client) {
    this._client = client
  }

  public async getLibraryEntry(type: string, path: string) {
    /* if type && path resolve to one entry, that entry is returned
       if type && path resolve to a directory, an array is returned */
    const allEntries = await this.getLibraryEntries(type)

    // is it a file?
    const entry = allEntries.find(e => e.path === path)
    if (entry) {
      return entry.data.body
    }

    // not a file, need to go through all library entries
    return this.parseDirectoryContents(allEntries, path)
  }

  protected parseDirectoryContents(
    allEntries: LibraryEntry[],
    path: string
  ): any[] {
    // Remove trailing & leading / of path
    path = path.trim()
    if (path.endsWith("/")) path = path.slice(0, path.length - 1)
    if (path.startsWith("/")) path = path.substr(1)

    // if it's a directory, need to return all entries in the directory
    const dirs: any[] = []
    const files: any[] = []

    allEntries.forEach(e => {
      // if the entry is not prefixed with the path, leave (be careful with "sub1/" & "sub11/")
      if (!e.path.match(new RegExp(`^${path || ".*"}($|\/)`))) return

      // slice off the prefix (prefix can be "")
      let pathSuffix = e.path.slice(path.length, e.path.length)

      // remove leading / of suffix
      if (pathSuffix.startsWith("/")) pathSuffix = pathSuffix.substr(1)

      // if suffix contains /, -> dir, otherwise, -> file
      if (/\//.test(pathSuffix)) {
        // this entry is a directory, "sub12/f.js" => "sub12"
        const substrings = pathSuffix.match(/^([^\/]+)\//)
        if (
          substrings &&
          substrings.length >= 2 &&
          substrings[1].length > 0 &&
          !dirs.includes(substrings[1])
        ) {
          dirs.push(substrings[1])
        }
      } else {
        // this entry is a file
        files.push({ fn: pathSuffix })
      }
    })

    return dirs.concat(files)
  }

  private async getLibraryEntries(type: string): Promise<LibraryEntry[]> {
    return this._client
      ?.performGetRequest("/storage/library-" + type)
      .then(response => {
        if (response) return response.data
        else return [] as LibraryEntry[]
      })
  }

  public async saveLibraryEntry(
    type: string,
    path: string,
    meta: object,
    body: object
  ) {
    const newEntry: LibraryEntry = {
      path: path,
      data: {
        meta: meta,
        body: body
      }
    }

    // get existing entries for this type
    const allEntries: LibraryEntry[] = await this.getLibraryEntries(type)

    // does an entry with this path already exist?
    const existingIndex = allEntries.findIndex(e => e.path === newEntry.path)

    if (existingIndex > -1) {
      allEntries[existingIndex] = newEntry // replace entry
    } else {
      allEntries.push(newEntry)
    }

    return this._client
      ?.performPutRequest("/storage/library-" + type, allEntries)
      .then(response => response.statusText)
  }
}

export const library: Library = new Library()

export async function getLibraryEntry(type: string, path: string) {
  return library.getLibraryEntry(type, path)
}

export async function saveLibraryEntry(
  type: string,
  path: string,
  meta: object,
  body: object
) {
  return library.saveLibraryEntry(type, path, meta, body)
}
