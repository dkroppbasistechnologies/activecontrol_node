import { Client } from "./client"

class RuntimeSettings {
  private _client: Client | undefined

  set client(client: Client) {
    this._client = client
  }

  public async getSettings() {
    return this._client
      ?.performGetRequest("/storage/settings")
      .then(response => {
        if (response) return response.data
        else return []
      })
  }

  public async saveSettings(settings: object) {
    return this._client
      ?.performPutRequest("/storage/settings", settings)
      .then(response => response.statusText)
  }
}

export const runtimeSettings: RuntimeSettings = new RuntimeSettings()

export async function getSettings() {
  return runtimeSettings.getSettings()
}
export async function saveSettings(settings: object) {
  return runtimeSettings.saveSettings(settings)
}
